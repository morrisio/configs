#!/bin/bash

if [ -d $HOME/.config/Code/User ]; then
    ln -snf ${PWD}/settings.json $HOME/.config/Code/User/settings.json
fi
