#!/bin/bash

if [ -d $HOME/.config/nvim ]; then
    ln -snf ${PWD}/.vimrc $HOME/.config/nvim/init.vim
fi

ln -snf ${PWD}/.vimrc $HOME/.vimrc
ln -snf ${PWD}/.vimrc $HOME/.ideavimrc

